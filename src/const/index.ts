import dotenv from "dotenv";

dotenv.config();

export const TWITCH_REDIRECT_URL = "http://localhost";
export const TWITCH_CLIENT_ID = process.env.twitch_client_id;
export const TWITCH_CLIENT_SECRET = process.env.twitch_client_secret;
