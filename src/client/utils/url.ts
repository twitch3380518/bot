export function getClientUrl() {
  let result = "http://localhost:62606";
  const env = process.env.NODE_ENV;

  if (env === "development") {
    result = "http://localhost:62606";
  }

  return result;
}
