import express from "express";
import { TWITCH_CLIENT_ID, TWITCH_CLIENT_SECRET } from "/src/const";
import { initMidleware, initRoute } from "/src/routes/";

const app = express();
const port = 80;

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});

initMidleware();
initRoute();

export { app };
