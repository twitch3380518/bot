FROM node
EXPOSE 80
COPY ./package.json /App/

WORKDIR /App/
RUN npm i
CMD ["npm","start"]